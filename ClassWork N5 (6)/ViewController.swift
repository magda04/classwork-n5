//
//  ViewController.swift
//  ClassWork N5 (6)
//
//  Created by Magdiel Altynbekov on 20/3/23.
//

import UIKit

class Person {
     var name: String?
    var apartment: Apartment?

    init(name: String) {
        self.name = name
        print("\(name) was initialized")
    }

    deinit {
        print("\(name) was deinitialized")
    }
}

class Apartment {
     var unit: String
      var tenant: Person?

    init(unit: String) {
        self.unit = unit
        print("Apartment \(unit) was initialized")
    }

    deinit {
        print("Apartment \(unit) was deinitialized")
    }
}


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Создание цикла сильных ссылок (Retain Cycle)
       weak var person: Person?
      weak  var apart: Apartment?

        person = Person(name: "John")
        apart = Apartment(unit: "4A")

        person?.apartment = apart
        apart?.tenant = person

        // Теперь мы не можем освободить ни один из объектов, потому что они ссылаются друг на друга
        person = nil
        apart = nil
    }


}

